//
//  HUDView.swift
//  Reverse Geocoding Example
//
//  Created by David van Enckevort on 21-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import UIKit

/**
 The HUD is the display part of the targeting device. It shows the location at which the device points.
 */
@IBDesignable final public class HUD: TargetingDevicePart {
    /**
     Width of the HUD border, default is 2.0 pt.
    */
    @IBInspectable var hudBorderWidth: CGFloat = 2.0 {
        didSet {
            layer.borderWidth = hudBorderWidth
        }
    }
    /**
     Radius of the corner of the border of the HUD, default is 10 pt.
     */
    @IBInspectable var hudCornerRadius: CGFloat = 10 {
        didSet {
            layer.cornerRadius = hudCornerRadius
        }
    }
}
