//
//  CrossHairsView.swift
//  Reverse Geocoding Example
//
//  Created by David van Enckevort on 20-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import UIKit

/**
 Custom view for a configurable cross hair positioning control.
 */
@IBDesignable final public class CrossHairs: TargetingDevicePart {
    /**
     Width of the lines of the cross hairs.
    */
    @IBInspectable var lineWidth: CGFloat = 2
    /**
     Radius of the inner circle of the cross hairs. Should be larger than the gap between the cross hairs as defined by `crossHairXGap` and crossHairYGap`.
     Default value is ⅙ of the lesser of the width and height of the view.
    */
    @IBInspectable lazy var radiusOfSmallCircle: CGFloat = length / 6
    /**
     Radius of the outer circle of the cross hairs. Should be larger than radius of the inner circle as defined by the `radiusOfSmallCircle`.
     Default value is ⅓ of the `length` of the view.
     */
    @IBInspectable lazy var radiusOfLargeCircle: CGFloat = length / 3
    /**
      The gap between the center or focus point of the cross hairs and the end of the horizontal cross hair. This should be smaller than the radius of the inner circle as defined by `radiusOfSmallCircle`.
      */
    @IBInspectable var crossHairXGap: CGFloat = 10
    /**
     The gap between the center or focus point of the cross hairs and the end of the vertical cross hair. This should be smaller than the radius of the inner circle as defined by `radiusOfSmallCircle`.
     */
    @IBInspectable var crossHairYGap: CGFloat = 10
    /**
     Starting angle of a full circle expressed in radians: 0
    */
    private let fullCircleStartAngle: CGFloat = 0.0
    /**
     Ending angle of a full circle expressed in radians: 2 × 𝛑
    */
    private let fullCircleEndAngle: CGFloat = 2 * CGFloat.pi
    /**
     * Length of the shortest edge of the bounds of this view.
     */
    private var length: CGFloat {
        return bounds.size.width < bounds.size.height ? bounds.size.height : bounds.size.width
    }

    override public func draw(_ rect: CGRect) {
        tintColor.setStroke()
        let path = UIBezierPath()
        path.lineWidth = lineWidth
        path.addArc(withCenter: bounds.center, radius: radiusOfLargeCircle, startAngle: fullCircleStartAngle, endAngle: fullCircleEndAngle, clockwise: true)
        path.addArc(withCenter: bounds.center, radius: radiusOfSmallCircle, startAngle: fullCircleStartAngle, endAngle: fullCircleEndAngle, clockwise: true)
        path.move(to: CGPoint(x: bounds.minX, y: bounds.midY))
        path.addLine(to: CGPoint(x: bounds.midX - crossHairXGap, y: bounds.midY))
        path.move(to: CGPoint(x: bounds.midX + crossHairXGap, y: bounds.midY))
        path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.midY))
        path.move(to: CGPoint(x: bounds.midX, y: bounds.minY))
        path.addLine(to: CGPoint(x: bounds.midX, y: bounds.midY - crossHairYGap))
        path.move(to: CGPoint(x: bounds.midX, y: bounds.midY + crossHairYGap))
        path.addLine(to: CGPoint(x: bounds.midX, y: bounds.maxY))
        path.stroke()
    }

}

private extension CGRect {
    /**
     Convenience property for the center or midpoint of this rectangle.
    */
    var center: CGPoint {
        return CGPoint(x: midX, y: midY)
    }
}
