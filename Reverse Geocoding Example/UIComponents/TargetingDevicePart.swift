//
//  TargetingDeviceElementView.swift
//  Reverse Geocoding Example
//
//  Created by David van Enckevort on 21-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import UIKit

/**
 Base class for parts of the targeting device UI elements to share designable properties.
 */
@IBDesignable public class TargetingDevicePart: UIView {

    /**
     Color of the shadow of this part. Default is black.
     */
    @IBInspectable var shadowColor: UIColor = .black {
        didSet {
            layer.borderColor = shadowColor.cgColor
        }
    }
    /**
     Opacity of the shadow. Default is 40%.
     */
    @IBInspectable var shadowOpacity: CGFloat = 0.40 {
        didSet {
            layer.shadowOpacity = Float(shadowOpacity)
        }
    }
    /**
     X Offset of the shadow. A negative number is to the left and positive number is to the right. Default is 5.0.
     */
    @IBInspectable var shadowXOffset: CGFloat = 5.0 {
        didSet {
            layer.shadowOffset = CGSize(width: shadowXOffset, height: shadowYOffset)
        }
    }
    /**
     Y Offset of the shadow. A negative number is above and positive number is below. Default is 5.0.
     */
    @IBInspectable var shadowYOffset: CGFloat = 5.0 {
        didSet {
            layer.shadowOffset = CGSize(width: shadowXOffset, height: shadowYOffset)
        }
    }
}
