//
//  ViewController.swift
//  Reverse Geocoding Example
//
//  Created by David van Enckevort on 19-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import os

/**
 Main ViewController showing a scrollable map with a targeting cross hair fixed at the center of the view.
 */
final class ViewController: UIViewController {
    /**
     The map view on which the user searches for a location.
     */
    @IBOutlet weak var map: MKMapView!
    /**
     The label on the HUD to show the address.
     */
    @IBOutlet weak var street: UILabel!
    /**
     The label on the HUD to show the city.
     */
    @IBOutlet weak var city: UILabel!
    /**
     The label on the HUD to show the country.
     */
    @IBOutlet weak var country: UILabel!
    /**
     Geocoder instance used to do the reverseGeocodeLocationing.
     */
    let geocoder = CLGeocoder()
    /**
     Location from the last call to the reverseGeocodeLocation function to prevent asking for the same location multiple times.
     */
    var previousLocation: CLLocation?
    /**
     Serial queue to ensure no parallel execution of reverseGeocodeLocation calls.
     */
    let geocoderQueue = DispatchQueue(label: "nl.allthingsdigital.reverse-geocoding")
    /**
     Location manager to request current location.
     */
    @IBOutlet weak var locatingView: UIStackView!
    @IBOutlet weak var locationDataView: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    lazy var locationManager: CLLocationManager = {
        var manager = CLLocationManager()
        manager.delegate = self
        return manager
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        map.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupLocationServices()
        updateHUD()
    }

    /**
     Setup the location services.
    */
    private func setupLocationServices() {
        /**
         Open settings.
        */
        func openLocationSettings(_ alert: UIAlertAction) {
            let locationSettings = URL(string: UIApplication.openSettingsURLString)!
            UIApplication.shared.open(locationSettings, options: [:], completionHandler: nil)
        }

        /**
         Show an alert to tell that location services are disabled.
        */
        func showAlert() {
            let alert = UIAlertController(title: "Location services are disabled", message: "Please enable the location services in the system preferences.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: openLocationSettings))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }

        guard CLLocationManager.locationServicesEnabled() else {
            showAlert()
            return
        }
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            showAlert()
        case .authorizedAlways, .authorizedWhenInUse:
            // We are authorized to request the location.
            requestLocation()
        }
    }

    private func requestLocation() {
        locationDataView.isHidden = true
        locatingView.isHidden = false
        activityIndicator.startAnimating()
        locationManager.requestLocation()
    }

    /**
     Update the HUD with the information on the geolocation. This will set the `street`, `city` and `country` UILabels.

     **NOTE:** The UILabels are guaranteed to be set from the main thread, so it is safe to call this method from another thread.
    */
    private func updateHUD() {
        /**
         Completion handler to actually update the hud.
         - Parameter placemarks: The array of `CLPlacemark` or nil.
         - Parameter error: The `Error` object or nil.
         */
        func updateHUD(_ placemarks: [CLPlacemark]?, _ error: Error?) {
            guard let placemarks = placemarks,
                let placemark = placemarks.first
                else { return }
            previousLocation = placemark.location
            country?.text = placemark.country
            city?.text = placemark.locality
            // if subThoroughfare is set we assume it is the number of the building
            // and prepend it to the street
            if let subThoroughfare = placemark.subThoroughfare {
                street?.text = "\(subThoroughfare) \(placemark.thoroughfare ?? "")"
            } else {
                street?.text = placemark.thoroughfare
            }
        }

        self.geocoderQueue.async {
            [weak self] in
            guard let self = self else { return }
            let location = self.map.centerCoordinate.location
            // Don't update if we only moved a little since the last time we geolocated, because CLGeocoder.reverseGeocodeLocation(_:completionHandler:) is rate limited.
            if let previousLocation = self.previousLocation, location.distance(from: previousLocation) < 50 {
                return
            }
            self.geocoder.reverseGeocodeLocation(location, completionHandler: updateHUD)
        }
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        updateHUD()
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            setupLocationServices()
        default:
            print("Location services were not authorized.")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location services were not authorized.")
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            print("No location was given.")
            return
        }
        map.setCenter(location.coordinate, animated: true)
        activityIndicator.stopAnimating()
        locatingView.isHidden = true
        locationDataView.isHidden = false
    }
}

private extension CLLocationCoordinate2D {
    /**
     Convenience variable to convert to CLLocation.
    */
    var location: CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
}
